### rainbird-js-sdk-demo

This repository serves as a simple demonstration of leveraging the capabilities of the **rainbird/sdk-react** package to build a straightforward use case. The Rainbird SDK for React provides integration of Rainbird's powerful AI reasoning engine into React applications, facilitating the creation of intelligent decision-making systems.

A common approach to using the Rainbird API is described in the diagram below.

1.  Start endpoint is called to start a new session.

2.  Query endpoint is called to perform a query. The response to query will contain either a question or a result.

3.  If a question has been returned, then the response endpoint should be called with an answer. This request may return another question.

4.  The response endpoint will return a result once the query has reached a conclusion.

![Common Rainbird Approach](/src/assets/image.png)

### Installation and Setup

To clone and run this repository locally, follow these steps:

1.  Clone the repository to your local machine using Git:
    
    `git clone https://github.com/your-username/rainbird-js-sdk-demo.git`
    
2.  Navigate to the project directory:
    
    `cd rainbird-js-sdk-demo`
    
3.  Install dependencies using Yarn:
    
    `yarn install`
    

### Usage

Once the repository is cloned and dependencies are installed, you can run the demo locally by executing the following command:

*   To start the development server:
    
    `yarn dev`
    
    This command starts the development server using Vite, allowing you to preview the application in your browser.
    
*   To build the application for production:
    
    `yarn build`
    
    This command compiles the application into a deployable format optimised for production.
    
*   To preview the production build locally:
    
    `yarn preview`
    
    This command serves the production build locally, allowing you to view the application as it would appear in a production environment.
    

### Issues

If you encounter any issues or have suggestions for improvements, please feel free to open an issue.

### License

This project is licensed under the MIT License - see the LICENSE file for details.
