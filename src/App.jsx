import { RainbirdProvider } from '@rainbird/sdk-react';

import { QueryWrapper, SessionForm, Start } from '@components';
import { useSession } from '@hooks';

import './App.css';

function App() {
  const { validSession, ...session } = useSession();
  const { apiKey, baseUrl, kmid, sessionID } = session;

  return validSession ? (
    <RainbirdProvider
      apiKey={apiKey}
      baseURL={baseUrl}
      kmID={kmid}
      sessionID={sessionID}
    >
      {sessionID ? <QueryWrapper /> : <Start />}
    </RainbirdProvider>
  ) : (
    <SessionForm session={session} />
  );
}

export default App;
