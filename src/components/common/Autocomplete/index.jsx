import { useState } from 'react';
import PropTypes from 'prop-types';

import './index.css';

export const Autocomplete = ({ options, onChange }) => {
  const [inputValue, setInputValue] = useState('');

  const handleInputChange = e => {
    const newValue = e.target.value;
    setInputValue(newValue);
    onChange(newValue);
  };

  return (
    <div>
      <input
        type="text"
        value={inputValue}
        onChange={handleInputChange}
        placeholder="Type to search"
        list="options-list"
      />
      <datalist id="options-list">
        {options.map(option => (
          <option key={option.name} value={option.name} />
        ))}
      </datalist>
    </div>
  );
};

Autocomplete.propTypes = {
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
};
