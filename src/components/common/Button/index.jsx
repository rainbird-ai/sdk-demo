import PropTypes from 'prop-types';

import './index.css';

export const Button = ({
  disabled = false,
  onClick,
  text,
  type = 'submit',
}) => (
  <button type={type} disabled={disabled} onClick={onClick}>
    {text}
  </button>
);

Button.propTypes = {
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  text: PropTypes.string.isRequired,
  type: PropTypes.string,
};
