import PropTypes from 'prop-types';

import './index.css';

export const Input = ({
  onChange,
  placeholder,
  required = false,
  type = 'text',
  value,
}) => (
  <input
    onChange={onChange}
    placeholder={placeholder}
    required={required}
    type={type}
    value={value}
  />
);

Input.propTypes = {
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  type: PropTypes.string,
  value: PropTypes.string,
};
