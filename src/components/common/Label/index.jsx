import PropTypes from 'prop-types';

import './index.css';

export const Label = ({ text }) => <label>{text}</label>;

Label.propTypes = {
  text: PropTypes.string.isRequired,
};
