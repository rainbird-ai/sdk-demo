import PropTypes from 'prop-types';

import './index.css';

export const Select = ({ value, options, onChange }) => (
  <select value={value || ''} onChange={e => onChange(e.target.value)}>
    <option value="" disabled hidden>
      Select an option
    </option>
    {options.map(option => (
      <option key={option.name} value={option.value}>
        {option.value}
      </option>
    ))}
  </select>
);

Select.propTypes = {
  value: PropTypes.string,
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
};
