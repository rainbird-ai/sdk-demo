import PropTypes from 'prop-types';

import { Button, Input, Label } from '@components';
import './index.css';

export const QueryForm = ({ goal }) => {
  const {
    handleSubmit,
    object,
    relationship,
    setObject,
    setRelationship,
    setSubject,
    subject,
  } = goal;

  return (
    <form onSubmit={handleSubmit} className="query-form">
      <div className="form-group">
        <Label text={'Subject:'} />
        <Input
          className="form-input"
          onChange={e => setSubject(e.target.value)}
          required
          value={subject}
        />
      </div>
      <div className="form-group">
        <Label text={'Relationship:'} />
        <Input
          className="form-input"
          onChange={e => setRelationship(e.target.value)}
          required
          value={relationship}
        />
      </div>
      <div className="form-group">
        <Label text={'Object:'} />
        <Input
          className="form-input"
          onChange={e => setObject(e.target.value)}
          value={object}
        />
      </div>
      <Button text="Make Query" className="submit-button" />
    </form>
  );
};

QueryForm.propTypes = { goal: PropTypes.object.isRequired };
