import { FormControl } from '@rainbird/sdk-react';
import PropTypes from 'prop-types';

import {
  Autocomplete,
  Input,
  Label,
  NotImplemented,
  Select,
} from '@components';
import './index.css';

export const QuestionForm = ({ handleChange, question, response, target }) => {
  const handleResponse = newValue => {
    const newResponse = response[0];
    newResponse[target] = newValue || undefined;
    return handleChange([newResponse]);
  };

  const getOptions = concepts => {
    if (!concepts) return [];
    return concepts.filter(concept => !concept.invalidResponse);
  };

  return (
    <div className="question-form">
      <Label text={question.prompt} />
      <div className="input-container">
        <FormControl
          data={question}
          multiString={<NotImplemented />}
          multiStringAdd={<NotImplemented />}
          singleDate={<NotImplemented />}
          singleNumber={<NotImplemented />}
          singleString={
            <Select
              value={response[0][target]}
              options={getOptions(question.concepts)}
              onChange={handleResponse}
            />
          }
          singleStringAdd={
            question.concepts && question.concepts.length > 0 ? (
              <Autocomplete
                value={response[0][target]}
                options={getOptions(question.concepts)}
                onChange={handleResponse}
              />
            ) : (
              <Input onChange={handleResponse} value={response[0][target]} />
            )
          }
          singleTruth={<NotImplemented />}
        />
      </div>
    </div>
  );
};

QuestionForm.propTypes = {
  handleChange: PropTypes.func.isRequired,
  response: PropTypes.array.isRequired,
  question: PropTypes.object.isRequired,
  target: PropTypes.string.isRequired,
};
