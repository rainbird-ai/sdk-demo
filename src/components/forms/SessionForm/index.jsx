import PropTypes from 'prop-types';

import { Button, Input, Label } from '@components';
import './index.css';

export const SessionForm = ({ session }) => {
  const {
    apiKey,
    baseUrl,
    handleSubmit,
    kmid,
    sessionID,
    setApiKey,
    setBaseUrl,
    setKmid,
    setSessionID,
  } = session;

  return (
    <form onSubmit={handleSubmit} className="session-form">
      <div className="form-group">
        <Label text={'URL:'} />
        <Input
          className="form-input"
          onChange={e => setBaseUrl(e.target.value)}
          required
          value={baseUrl}
        />
      </div>
      <div className="form-group">
        <Label text={'API Key:'} />
        <Input
          className="form-input"
          onChange={e => setApiKey(e.target.value)}
          required
          value={apiKey}
        />
      </div>
      <div className="form-group">
        <Label text={'Knowledge Map ID:'} />
        <Input
          className="form-input"
          onChange={e => setKmid(e.target.value)}
          required
          value={kmid}
        />
      </div>
      <div className="form-group">
        <Label text={'Session ID:'} />
        <Input
          className="form-input"
          onChange={e => setSessionID(e.target.value)}
          placeholder="Optional"
          value={sessionID}
        />
      </div>
      <Button text="Start Session" className="submit-button" />
    </form>
  );
};

SessionForm.propTypes = { session: PropTypes.object.isRequired };
