import { useUndo, useResponse } from '@rainbird/sdk-react';
import PropTypes from 'prop-types';

import { Button, QuestionForm } from '@components';
import { useInteractState } from '@hooks';
import './index.css';

export const Interact = ({ data }) => {
  const { question, extraQuestions } = data;
  const initialState = data.question
    ? [
        [
          {
            cf: 100,
            object: question.object,
            relationship: question.relationship,
            subject: question.subject,
          },
        ],
        ...extraQuestions.map(({ subject, relationship, object }) => [
          { subject, relationship, object, cf: 100 },
        ]),
      ]
    : undefined;

  const { response, updateResponse, flattenResponse } =
    useInteractState(initialState);
  const respond = useResponse();
  const undo = useUndo();

  const getQuestionTarget = q => {
    const qType = q?.type.toLowerCase();
    if (qType === 'first form') return 'answer';
    return qType === 'second form object' ? 'object' : 'subject';
  };

  const submitDisabled = response?.some(group =>
    group.some((responseItem, groupIndex) => {
      const currentQuestion =
        groupIndex > 0 ? extraQuestions[groupIndex - 1] : question;
      const targetValue = responseItem[getQuestionTarget(currentQuestion)];
      const hasKnownAnswers =
        Array.isArray(currentQuestion.knownAnswers) &&
        currentQuestion.knownAnswers.length;

      if (
        (targetValue === undefined && currentQuestion.allowUnknown) ||
        hasKnownAnswers
      )
        return false;
      return targetValue === undefined;
    })
  );

  const submit = e => {
    e.preventDefault();
    if (submitDisabled) return;
    const res = flattenResponse().map(handleUnanswered);
    respond(res);
  };

  const handleUnanswered = response => {
    const { subject, object, ...rest } = response;
    const isSubjectUnanswered = !subject && typeof subject !== 'boolean';
    const isObjectUnanswered = !object && typeof object !== 'boolean';

    if (isSubjectUnanswered || isObjectUnanswered)
      return { ...rest, unanswered: true };

    return response;
  };

  const handleSkip = () =>
    respond([
      {
        cf: 100,
        object: question.object || '',
        relationship: question.relationship,
        subject: question.subject || '',
        unanswered: true,
      },
      ...extraQuestions.map(({ subject, relationship, object }) => [
        {
          cf: 100,
          object,
          relationship,
          subject,
          unanswered: true,
        },
      ]),
    ]);

  return (
    <form onSubmit={submit}>
      <QuestionForm
        handleChange={group => updateResponse(group, 0)}
        question={question}
        response={response[0]}
        target={getQuestionTarget(question)}
      />
      {extraQuestions?.map((question, i) => (
        <QuestionForm
          handleChange={group => updateResponse(group, i + 1)}
          key={`extraQuestionGroup-${i}`}
          question={question}
          response={response[i + 1]}
          target={getQuestionTarget(question)}
        />
      ))}
      <div className="button-container">
        <Button
          className="back-button"
          onClick={undo}
          text="Back"
          type="button"
        />
        {question.allowUnknown && (
          <Button
            className="skip-button"
            onClick={handleSkip}
            text="Skip"
            type="button"
          />
        )}
        <Button
          className="submit-button"
          disabled={submitDisabled}
          text="Submit"
        />
      </div>
    </form>
  );
};

Interact.propTypes = { data: PropTypes.object.isRequired };
