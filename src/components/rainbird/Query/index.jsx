import { useQuery } from '@rainbird/sdk-react';
import PropTypes from 'prop-types';

export const Query = ({ subject, relationship, object, children }) => {
  useQuery(subject, relationship, object);

  return children;
};

Query.propTypes = {
  children: PropTypes.element.isRequired,
  object: PropTypes.string,
  relationship: PropTypes.string.isRequired,
  subject: PropTypes.string.isRequired,
};
