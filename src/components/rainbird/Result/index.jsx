import PropTypes from 'prop-types';

import './index.css';

export const Result = ({ data }) => {
  if (!data || data.length === 0) {
    return (
      <p className="result-item">
        Sorry, I&apos;ve been unable to find an answer to your question!
      </p>
    );
  }

  return (
    <div className="result-container">
      <h2>Your results:</h2>
      <ul className="results-list">
        {data.map((result, index) => (
          <li key={index} className="result-item">
            {`${result.subject} ${result.relationship} ${result.object} - ${result.certainty}%`}
          </li>
        ))}
      </ul>
    </div>
  );
};

Result.propTypes = { data: PropTypes.array.isRequired };
