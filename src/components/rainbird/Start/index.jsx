import { useStart } from '@rainbird/sdk-react';

import { Error, Loading, QueryWrapper } from '@components';

export const Start = () => {
  const { data, error, loading } = useStart();

  return (
    <>
      {loading && <Loading />}
      {error && <Error />}
      {data && <QueryWrapper />}
    </>
  );
};
