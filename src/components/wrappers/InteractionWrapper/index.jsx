import { InteractionProvider } from '@rainbird/sdk-react';
import PropTypes from 'prop-types';

import { Error, Loading, Query } from '@components';

export const InteractionWrapper = ({ children, goal }) => {
  const { object, relationship, subject } = goal;

  return (
    <InteractionProvider>
      {({ data, loading, error }) => (
        <Query subject={subject} relationship={relationship} object={object}>
          <>
            {loading && <Loading />}
            {error && <Error />}
            {data && children({ type: data.type, data: data.data })}
          </>
        </Query>
      )}
    </InteractionProvider>
  );
};

InteractionWrapper.propTypes = {
  children: PropTypes.func.isRequired,
  goal: PropTypes.object.isRequired,
};
