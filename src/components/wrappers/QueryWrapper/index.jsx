import { RESPONSE_TYPE_QUESTION, RESPONSE_TYPE_RESULT } from '@rainbird/sdk';

import { InteractionWrapper, Interact, Result, QueryForm } from '@components';
import { useGoal } from '@hooks';

export const QueryWrapper = () => {
  const { validGoal, ...goal } = useGoal();

  return validGoal ? (
    <InteractionWrapper goal={goal}>
      {({ type, data }) => {
        if (type === RESPONSE_TYPE_QUESTION) return <Interact data={data} />;
        if (type === RESPONSE_TYPE_RESULT) return <Result data={data} />;
      }}
    </InteractionWrapper>
  ) : (
    <QueryForm goal={goal} />
  );
};
