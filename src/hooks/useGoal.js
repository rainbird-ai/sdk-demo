import { useState } from 'react';

export const useGoal = () => {
  const [subject, setSubject] = useState('Rachel');
  const [relationship, setRelationship] = useState('speaks');
  const [object, setObject] = useState('');
  const [validGoal, setValidGoal] = useState(false);

  const handleSubmit = event => {
    event.preventDefault();
    if (subject && relationship) setValidGoal(true);
  };

  return {
    handleSubmit,
    object,
    relationship,
    setObject,
    setRelationship,
    setSubject,
    subject,
    validGoal,
  };
};
