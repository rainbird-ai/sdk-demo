import { useState } from 'react';

export const useInteractState = initialData => {
  const [response, setResponse] = useState(initialData);

  const updateResponse = (group, index) =>
    setResponse(prevResponse => {
      const newResponse = [...prevResponse];
      newResponse[index] = group;
      return newResponse;
    });

  const flattenResponse = () =>
    response.reduce((result, group) => result.concat(group), []);

  const resetState = () => setResponse(initialData);

  return {
    response,
    updateResponse,
    flattenResponse,
    resetState,
  };
};
