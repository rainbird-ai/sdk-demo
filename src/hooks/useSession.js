import { useState } from 'react';

export const useSession = () => {
  const [apiKey, setApiKey] = useState('37582f71-b9b1-4451-b9e6-c130c7fd2412');
  const [baseUrl, setBaseUrl] = useState('https://api.rainbird.ai');
  const [kmid, setKmid] = useState('2fd1be28-b38d-4fa3-8b9d-b0976821912c');
  const [sessionID, setSessionID] = useState('');
  const [validSession, setValidSession] = useState(false);

  const handleSubmit = event => {
    event.preventDefault();
    if (apiKey && baseUrl && kmid) setValidSession(true);
  };

  return {
    apiKey,
    baseUrl,
    handleSubmit,
    kmid,
    sessionID,
    setApiKey,
    setBaseUrl,
    setKmid,
    setSessionID,
    validSession,
  };
};
